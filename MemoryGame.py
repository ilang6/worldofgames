
import random as rnd
from time import sleep
from os import system

# setting Difficulty

difficulty = 4

# generating sequence of number


def generate_sequence(difficulty_input):
    rnd_list = rnd.sample(range(1, 101), difficulty_input)
    return rnd_list


# getting a list of numbers from user


def get_list_from_user(difficulty_input):
    user_list_input = input("Please enter "+str(difficulty_input)+" numbers")
    user_list = user_list_input.split()
    while len(user_list) != difficulty_input:
        if len(user_list) != difficulty_input:
            print("You have entered "+str(len(user_list))+" values please enter "+str(difficulty_input)+" values")
            user_list_input = input("Please enter " + str(difficulty_input) + " numbers")
            user_list = user_list_input.split()
    else:
        return user_list


def is_list_equal(random_seq, user_input):
    rnd_list = random_seq
    user_seq = user_input
    user_seq = list(map(int, user_seq))
    if rnd_list == user_seq:
        result = True
        return result
    else:
        result = False
        return result


def play(difficulty_input):
    rnd_list = generate_sequence(difficulty_input)
    print("the numbers are: "+str(rnd_list))
    sleep(0.7)
    system('clear')
    user_list = get_list_from_user(difficulty_input)
    result = is_list_equal(rnd_list, user_list)
    return result
