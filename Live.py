from GuessGame import play as gplay
from MemoryGame import play as mplay
from CurrencyRouletteGame import play as cplay

# welcome message

def welcome():
    name = input("Please tell us your name")
    print("Hello " + name + " and welcome to the World of Games (WoG)\n"
                            "Here you can find many cool games to play.")

# game of choose and difficulty


def load_game():
    from pandas import DataFrame, options

    options.display.max_colwidth = 100

    game_dict = {
        1: "Memory Game - a sequence of numbers will appear for 1 second and you have to guess it back",
        2: "Guess Game - guess a number and see if you chose like the computer",
        3: "Currency Roulette - try and guess the value of a random amount of USD in ILS"
    }

    diff_dict = {
        1: "super easy",
        2: "easy",
        3: "normal",
        4: "expert",
        5: "bad ass"

    }

    print("Those are our games:")
    print(DataFrame.from_dict(game_dict, orient='index', columns=["Description"]))
    game_num = int(input("Please choose a game to play:"))

    if 1 <= game_num <= 3:
        print("choose one of the following difficulties:")
        print(DataFrame.from_dict(diff_dict, orient='index', columns=["Description"]))
        diff_num = int(input("Please make your selection now:"))
        if 1 <= diff_num <= 5:
            print("The game will begin shortly")
            if game_num == 1:
                result = mplay(diff_num)
                print(result)
            elif game_num == 2:
                result = gplay(diff_num)
                print(result)
            elif game_num == 3:
                result = cplay(diff_num)
                print(result)
        else:
            print("please chose a a valid difficultly")
    else:
        print("You did not choose a valid game please choose one")
