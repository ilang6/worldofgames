FROM python:latest

LABEL ADMIN Ilan_Gofer 
ENV DEBIAN_FRONTEND noninteractive

RUN apt update
RUN apt-get install -y \
                    git \
                    unixodbc* \
                    wget 

RUN apt-get install -y unixodbc*
RUN apt-get install -y unixodbc-dev

RUN wget https://dev.mysql.com/get/Downloads/Connector-ODBC/8.0/mysql-connector-odbc-8.0.17-linux-debian9-x86-64bit.tar.gz
RUN tar xvf mysql-connector-odbc-8.0.17-linux-debian9-x86-64bit.tar.gz
RUN cp /mysql-connector-odbc-8.0.17-linux-debian9-x86-64bit/bin/* /usr/local/bin
RUN cp /mysql-connector-odbc-8.0.17-linux-debian9-x86-64bit/lib/* /usr/local/lib
RUN /usr/local/bin/myodbc-installer -a -d -n "MySQL ODBC 8.0 Driver" -t "Driver=/usr/local/lib/libmyodbc8w.so"
RUN /usr/local/bin/myodbc-installer -a -d -n "MySQL ODBC 8.0" -t "Driver=/usr/local/lib/libmyodbc8a.so"

RUN git clone https://gitlab.com/ilang6/worldofgames.git

WORKDIR /worldofgames

RUN pip install -r requirements.txt

CMD ["python","MainScores.py"]


